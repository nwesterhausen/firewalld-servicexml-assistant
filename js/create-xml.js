/**
 * Created by nicholaswesterhausen on 7/3/2017.
 *
 * This javascript takes inputs from the form and outputs the XML into the #output field
 */


const XML_HEADER = '<?xml version="1.0" encoding="utf-8"?>\n';

function buildXML() {
    var outputXML = "";

    var name = $("#serviceName").val();
    var desc = $("#serviceDesc").val();
    var ports = getTableData("#portTableBody"); // (port, protocol)
    var sourcePorts = getTableData("#sourceportTableBody"); // (port, protocol)
    var protocols = getTableData("#protocolTableBody"); // (protocol)
    var modules = getTableData("#moduleTableBody"); // (module)
    var ipv4dest = $("#destinationIPV4").val();
    var ipv6dest = $("#destinationIPV6").val();

    var i = 0;

    outputXML += XML_HEADER;
    outputXML += "<service>\n";
    if (name.length > 0)
        outputXML += "\t<short>"+name+"</short>\n";
    if (desc.length > 0)
        outputXML += "\t<description>"+desc+"</description>\n";
    for (i=0; i<ports.length; i++) {
        outputXML += "\t<port port=\""+ports[i][0]+"\" protocol=\""+ports[i][1]+"\" />\n"
    }
    for (i=0; i<sourcePorts.length; i++) {
        outputXML += "\t<source-port port=\""+ports[i][0]+"\" protocol=\""+ports[i][1]+"\" />\n"
    }
    for (i=0; i<protocols.length; i++) {
        outputXML += "\t<protocol value=\""+protocols[i][0]+"\" />\n"
    }
    for (i=0; i<modules.length; i++) {
        outputXML += "\t<module name=\""+modules[i]+"\" />\n"
    }
    if (ipv4dest.length + ipv6dest.length > 0) {
        outputXML += "\t<destination ";
        if (ipv4dest.length > 0)
            outputXML += "ipv4=\""+ipv4dest+"\" ";
        if (ipv6dest.length > 0)
            outputXML += "ipv6=\""+ipv6dest+"\" ";
        outputXML += "/>\n";
    }
    outputXML += "</service>";
    $("#output").text(outputXML)
}

function getTableData(tbodyID) {
    var arr = [];
    $(tbodyID+" tr").each(function (i, row){
        var cells = $(row).find("td");
        var celldata = [];
        for (var i = 0; i < cells.length; i++)
            celldata.push(cells[i].textContent)
        console.log(tbodyID+":",celldata);
        arr.push(celldata)
    });
    return arr;
}