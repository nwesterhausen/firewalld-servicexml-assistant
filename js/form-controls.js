/**
 * Created by nicholaswesterhausen on 7/3/2017.
 *
 * This script controls the fieldsets of the form, and also enables the tables in the form.
 */

function addCollapseListener(fieldset, btnid) {
    $(fieldset).on('hide.bs.collapse',function() {
        var btn =$(btnid);
        btn.removeClass("btn-primary");
        btn.addClass("btn-default");
        console.log("collapse hid",fieldset);
    });
    $(fieldset).on('show.bs.collapse',function() {
        var btn =$(btnid);
        btn.removeClass("btn-default");
        btn.addClass("btn-primary");
        console.log("collapse showed",fieldset);
    });
    $(fieldset).collapse();
}

function addProtocol() {
    var $protocolInput = $("#protocolSel");
    var protocol = $protocolInput.val();
    $protocolInput.val('');
    $("#protocolTableBody").append(
        $.parseHTML("<tr><td>"+protocol+"</td></tr>"));
    $("#protocoltable").show();
}
function addModule() {
    var $moduleInput = $("#moduleSel");
    var module = $moduleInput.val();
    $moduleInput.val('');
    $("#moduleTableBody").append(
        $.parseHTML("<tr><td>"+module+"</td></tr>"));
    $("#moduleTable").show();
}
function addPort() {
    var $portInput = $("#portNumSel");
    var $protocolSel = $("input[name=portProtSel]:checked");
    var port = $portInput.val();
    $portInput.val('');
    var protocol = $protocolSel.val();
    $protocolSel.prop("checked",false);
    $("label[data-for=portProtSel]").removeClass("active");
    $("#portTableBody").append(
        $.parseHTML("<tr>" +
            "<td>"+port+"</td>" + "<td>"+protocol+"</td>" +
            "</tr>"));
    $("#portTable").show();
}
function addSourcePort() {
    var $portInput = $("#sourceportNumSel");
    var $protocolSel = $("input[name=sourceportProtSel]:checked");
    var port = $portInput.val();
    $portInput.val('');
    var protocol = $protocolSel.val();
    $protocolSel.prop("checked",false);
    $("label[data-for=sourceportProtSel]").removeClass("active");
    $("#sourceportTableBody").append(
        $.parseHTML("<tr>" +
            "<td>"+port+"</td>" + "<td>"+protocol+"</td>" +
            "</tr>"));
    $("#sourceportTable").show();
}

$(document).ready(function() {
    addCollapseListener("#basic-field","#basic-switch");
    addCollapseListener("#port-field","#port-switch");
    addCollapseListener("#source-port-field","#source-port-switch");
    addCollapseListener("#protocol-field","#protocol-switch");
    addCollapseListener("#destination-field","#destination-switch");
    addCollapseListener("#module-field","#module-switch");

    $("label[data-for]").css("margin-right","10px");
});
