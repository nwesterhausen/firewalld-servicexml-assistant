This is a HTML and Javascript GUI for creating service.xml for firewalld.

## How to use

1. Clone repo
1. Open index.html in your browser of choice
1. Fill in the form
1. Click "Update Output" to generate the XML
1. Copy/Paste the XML into a service.xml file

## Planned Updates

- A 'lite' version which run pure HTML5/JS (no jquery or bootstrap dependecies)
- A 'download' button which will download the generated XML as a .XML file